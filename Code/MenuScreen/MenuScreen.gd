extends Control
class_name MenuScreen

@onready var restart = $Restart
@onready var cat_select = $CatSelect
@onready var help = $Help
@onready var settings = $Settings
@onready var skip = $Skip

# Called when the node enters the scene tree for the first time.
func _ready():
	restart.pressed.connect(OnRestart)
	#cat_select.pressed.connect(OnCatSelect)
	help.pressed.connect(OnHelp)
	settings.pressed.connect(OnSettings)
	
func OnRestart():
	pass
#
#func OnCatSelect():
	#get_tree().change_scene_to_file(""
	#
func OnHelp():
	get_tree().change_scene_to_file("res://MenuScreen/HelpScreen.tscn")
func OnSettings():
	pass
		

	
