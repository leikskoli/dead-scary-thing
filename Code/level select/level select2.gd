extends Control


@onready var lvl_1 = $lvl1
@onready var lvl_2 = $lvl2
@onready var lvl_3 = $lvl3
@onready var lvl_4 = $lvl4
@onready var lvl_5 = $lvl5
@onready var lvl_6 = $lvl6
@onready var lvl_7 = $lvl7
@onready var lvl_9 = $lvl9



# Called when the node enters the scene tree for the first time.
func _ready():
	lvl_5.pressed.connect(Onlvl5)
	lvl_7.pressed.connect(Onlvl7)
	lvl_9.pressed.connect(Onlvl9)



func Onlvl5():
	print("SqurrielFlight selected")
	get_tree().change_scene_to_file("res ")
		

func Onlvl7():
	print("JayFeather selected")
	get_tree().change_scene_to_file("res://Slate/Slate.tscn")
	
func Onlvl9():
	print("ShadowSight Selected")
	get_tree().change_scene_to_file("res://Slate/Slate.tscn")
