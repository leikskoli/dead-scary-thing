extends Node2D
class_name Herbspawnerr
@onready var timer = $Timer
const HERB = preload("res://Herb/Herb.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	timer.timeout.connect(OnSpawn)
	pass # Replace with function body.


func OnSpawn():
	var herb = HERB.instantiate()
	add_child(herb)
