extends CharacterBody2D
class_name Player

const SPEED = 300.0
const JUMP_VELOCITY = -400.0
@onready var character = $Character

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")


func _ready():
	var child: Sprite2D
	var rando: int = randi_range(0, character.get_child_count() - 1)
	print(rando)
	for i in character.get_child_count():
		if i == rando:
			#character.get_child(i).show()
			child = character.get_child(i)
			child.show()
		else:
			child = character.get_child(i)
			child.hide()
	


func _physics_process(delta):
	for i in get_slide_collision_count():
		var c = get_slide_collision(i)
		if c.get_collider() is Herb:
			c.get_collider().queue_free()
	
	# Add the gravity.
	if not is_on_floor():
		velocity.y += gravity * delta

	# Handle Jump.
	if Input.is_action_just_pressed("ui_accept") and is_on_floor():
		velocity.y = JUMP_VELOCITY

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var direction = Input.get_axis("ui_left", "ui_right")
	if direction:
		velocity.x = direction * SPEED
		if velocity.x < 0:
			character.scale.x = 1
		else:
			character.scale.x = -1
			
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		#scale.x = 1

	move_and_slide()
	
	
	
	
