extends Control

@onready var progress_bar = $ProgressBar

func _process(delta):
	progress_bar.value = progress_bar.value - 1* delta
	if progress_bar.value == 0:
		get_tree().quit()
	
