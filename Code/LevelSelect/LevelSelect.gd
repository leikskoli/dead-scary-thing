extends Control
class_name LevelSelect

@onready var ls_1 = $LS1
@onready var ls_2 = $LS2
@onready var ls_3 = $LS3
@onready var ls_4 = $LS4

# Called when the node enters the scene tree for the first time.
func _ready():
	ls_1.pressed.connect(OnLS1)
	ls_2.pressed.connect(OnLS2)
	ls_3.pressed.connect(OnLS3)
	ls_4.pressed.connect(OnLS4)

func OnLS1():
	print("Play Level 1!")
	get_tree().change_scene_to_file("res://Levels/Level1.tscn")

func OnLS2():
	print("Play level 2!")
	get_tree().change_scene_to_file("res://Levels/Level2.tscn")
	
func OnLS3():
	print("Play Level 3!")
	get_tree().change_scene_to_file("res://Levels/Level 3.tscn")
	
func OnLS4():
	print("Play Level 4!")
	get_tree().change_scene_to_file("res://Levels/Level 4.tscn")




	
func OnLS5():
	print("Play level 5!")
	
func OnLS6():
	print("Play level 6!")
	
	
func OnLS7():
	print("Play level 7!")
	
func OnLS8():
	print("Play level 8!")
	
	
	
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
